def grating(raw, grating_id):
	# nonlocals
	response = None
	calibrated = None


	# functions
	def load_grating_response():
		import os
		import txt
		import system
		nonlocal response

		s = system.sep()
		f_name = os.getcwd().split(s)
		f_name.extend(['lib', 'etc', 'grating_efficiencies', grating_id])
		f_name = s.join(f_name)

		raw_data = txt.fetch_sep(f_name + '.txt', '\t')[1:]
		response = [(float(_rd[0]), float(_rd[1])) for _rd in raw_data]

		return

	def extrapolate_short(wl):
		import fit

		a, b = fit.lin(response[:2])

		return a * wl + b

	def extrapolate_long(wl):
		import fit

		a, b = fit.lin(response[-2:])

		return a * wl + b

	def interpolate(wl):
		import fit

		pos = -1
		for _r in response:
			if wl < _r[0]:
				break

			pos += 1

		a, b = fit.lin(response[pos:pos + 2])

		return a * wl + b

	def calibrate():
		nonlocal calibrated

		lim_short = response[0][0]
		lim_long = response[-1][0]
		calibrated = []
		for _r in raw:
			wl = _r[0]
			cnts = _r[1]
			calibration_factor = None
			if wl < lim_short:
				calibration_factor = extrapolate_short(wl)

			if wl > lim_long:
				calibration_factor = extrapolate_long(wl)

			if calibration_factor is None:
				calibration_factor = interpolate(wl)

			calibrated.append((wl, 100 * cnts / calibration_factor))

		return


	# main
	load_grating_response()
	calibrate()

	return calibrated
