def integrate(x, y):
	val = sum([(x[i] - x[i - 1]) * (y[i] + y[i - 1]) / 2 for i in range(x.__len__()) if i])
	
	return val