def show(img, t=1.0):
	import cv2

	t_ms = int(t * 1000)
	cv2.imshow('image under test', img)
	cv2.waitKey(t_ms)

	return
