def overlay(path, bg_name='background.bmp', background_scaling_factor=0, cutoff=250, gamma_removal_window_width=2, overlay_col=[1, 0, 0]):
	# nonlocals
	background = None
	overlay= None


	# functions
	def fetch_background():
		import sys
		import system
		import image

		nonlocal background

		# construct path
		sep = system.sep()
		p = path.split(sep)
		p = p[:-2]
		p.append(bg_name)
		p = sep.join(p)

		# fetch background
		background = image.read(p)

		# scale the background
		background.setflags(write=1)
		if not background_scaling_factor: return

		sys.stdout.write('\rScaling background image... ')
		for i in range(background.shape[0]):
			for j in range(background.shape[1]):
				val = background[i, j][:3]
				val = [min(it * background_scaling_factor, 255) for it in val]
				val.append(255)
				background[i, j] = val

		sys.stdout.write('done\n')

		# save scaled background
		p = p.split(sep)
		p = p[:-1]
		p.append('background_scaled.png')
		p = sep.join(p)

		image.save(background, p)

		return

	def show(tar):
		import image

		image.show(tar)

		return

	def remove_gamma(im):
		import numpy as np

		ret = im.copy()
		for i in range(im.shape[0]):
			for j in range(im.shape[1]):
				if ret[i, j] >= cutoff:
					top = max(i - gamma_removal_window_width, 0)
					bottom = min(i + gamma_removal_window_width + 1, im.shape[0])
					left = max(j - gamma_removal_window_width, 0)
					right = min(j + gamma_removal_window_width + 1, im.shape[1])
					avg_area = im[top : bottom, left : right]
					ret[i, j] = np.median(avg_area.reshape(-1))


		return ret

	def average_overlay(div_factor):
		import numpy as np
		import image

		nonlocal overlay

		overlay = np.divide(overlay, div_factor)
		for i in range(overlay.shape[0]):
			for j in range(overlay.shape[1]):
				if overlay[i, j] < cutoff: overlay[i, j] = 0

		return

	def iterate_through_images():
		import os
		import sys
		import image
		import numpy as np

		nonlocal overlay

		image_list = os.listdir(path)
		l = image_list.__len__()
		for i in range(l):
			sys.stdout.write('\rProcessing image ' + str(i + 1) + ' of ' + str(l))

			im = image.read(path + image_list[i])
			im = remove_gamma(im)
			if overlay is None: overlay = im
			else: overlay = np.add(overlay, im)

		sys.stdout.write('\n')

		average_overlay(l)

		return

	def apply_overlay():
		import image

		nonlocal background

		norm = max(overlay[int(overlay.shape[0] / 2):, :].reshape(-1)) / 255
		for i in range(overlay.shape[0]):
			if i < int(overlay.shape[0] / 2): continue
			for j in range(overlay.shape[1]):
				val = overlay[i, j]
				if val:
					col = [it * val / norm for it in overlay_col]
					col.append(255)
					background[i, j] = col

		return

	def save_complete_image():
		import system
		import image

		# construct path
		sep = system.sep()
		p = path.split(sep)
		p = p[:-2]
		p.append('overlay.png')
		p = sep.join(p)

		# save image
		image.save(background, p)

		return


	# main
	fetch_background()
	iterate_through_images()
	apply_overlay()
	save_complete_image()

	return
