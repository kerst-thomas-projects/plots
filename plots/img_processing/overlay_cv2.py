def overlay_cv2(path_src, path_tar, background_scaling_factor=0, centre=(0, 0), collection_radius=0):
	# nonlocals
	background = None
	individual_images = None
	compound_image = None


	# function
	def fetch_and_scale_background():
		import os
		import sys
		import cv2
		import system

		nonlocal background


		# get the image
		sep = system.sep()
		bg_path = path_src + sep + 'background.bmp'
		if not os.path.isfile(bg_path):
			sys.stdout.write('\rUnable to locate file: ' + bg_path + '. Abort program execution\n')
			sys.exit(1)

		background = cv2.imread(bg_path, 0)


		# scale the image
		if background_scaling_factor: background = cv2.multiply(background, background_scaling_factor)


		# save the new background image
		save_path = path_tar + sep + 'background_' + str(background_scaling_factor) + '_.bmp'
		cv2.imwrite(save_path, background)

		return

	def fetch_individual_images():
		# functions
		def load_into_memory():
			import cv2
			import numpy as np

			nonlocal individual_images

			individual_images = []

			files = os.listdir(im_path)
			for it in files:
				f_path = im_path + it
				im = cv2.imread(f_path, -1)
				if im is None:
					sys.stdout.write('\033[2J\r' + 'Unable to load image: ' + f_path + '. Aborting program execution\n')
					sys.exit(1)

				individual_images.append(im.copy())

			return


		# main
		import os
		import sys
		import system

		sep = system.sep()
		im_path = path_src + sep + '60s' + sep
		if not os.path.isdir(im_path):
			sys.stdout.write('\033[2J\r' + 'Unable to locate individual image files at ' + im_path + '. Aborting program execution\n')
			sys.exit(1)

		load_into_memory()

		return

	def compound_images():
		import system
		import cv2
		import numpy as np

		nonlocal compound_image

		sep = system.sep()
		for i, it in enumerate(individual_images):
			it = cv2.medianBlur(it, 7)

			if compound_image is None: compound_image = it.copy()
			else: compound_image = cv2.add(compound_image, it)

			it = cv2.multiply(it, individual_images.__len__())
			f_path =  path_tar + sep + str(i).zfill(2) + '.bmp'

			# scale image
			im = it.copy()
			im = im.astype(np.float32)
			im = cv2.multiply(im, 2 ** 8 - 1)
			im = cv2.divide(im, 2 ** 14 - 1)
			im = im.astype(np.uint8)
			cv2.imwrite(f_path, im)

		max_val = 255 / max(compound_image.reshape(-1))
		save_image = cv2.multiply(compound_image, max_val)
		cv2.imwrite(path_tar + sep + 'compound.bmp',  save_image)

		compound_image = compound_image.astype(np.float32)
		compound_image = cv2.divide(compound_image, individual_images.__len__())

		return

	def fetch_roi_val():
		import cv2
		import numpy as np

		nonlocal compound_image

		min_val = np.median(compound_image.reshape(-1))
		compound_image = np.subtract(compound_image, min_val)

		vals = []
		for i in range(compound_image.shape[0]):
			for j in range(compound_image.shape[1]):
				if np.sqrt((i - centre[1]) ** 2 + (j - centre[0]) ** 2) < collection_radius:
					vals.append(compound_image[i, j])
					compound_image[i, j] = 0

		return (np.mean(vals), np.std(vals))


	# main
	fetch_and_scale_background()
	fetch_individual_images()
	compound_images()
	avg_val = fetch_roi_val()

	return avg_val
