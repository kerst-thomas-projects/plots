def read(f_path):
    # nonlocals
    raw_data = []
    return_values = []

    # functions
    def parse_file():
        with open(f_path, 'r') as f:
            for line in f:
                year = int(line[:4])
                month = int(line[5:7])
                day = int(line[8:10])
                hour = int(line[11:13])
                minute = int(line[14:16])
                second = int(line[17:19])
                musecond = int(line[20:26])
                count = int(line[27:])
                raw_data.append({'year':year, 'month':month, 'day':day,
                    'hour':hour, 'minute':minute, 'second':second, 'musecond':musecond,
                    'count':count})

        return

    def remove_offset():
        nonlocal raw_data

        for key in raw_data[0].keys():
            if key == 'count':
                continue

            offset = raw_data[0][key]
            for i in range(raw_data.__len__()):
                raw_data[i][key] -= offset


        return

    def compile_return_values():
        for it in raw_data:
            # correct for exact month number
            t = it['year'] * 60 * 60 * 24 * 30 * 365 \
                + it['month'] * 60 * 60 * 24 * 30 \
                + it['day'] * 60 * 60 * 24 \
                + it['hour'] * 60 * 60 \
                + it['minute'] * 60 \
                + it['second'] \
                + it['musecond'] * 1e-6

            return_values.append((t, it['count']))

        return


    # main
    parse_file()
    remove_offset()
    compile_return_values()

    return return_values
