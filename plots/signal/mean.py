from .walk import walk


# Functions
def mean(input, filter_width=1):
	ret = walk(input, fun='mean', filter_width=filter_width)

	return ret
