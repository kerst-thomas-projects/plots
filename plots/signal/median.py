from .walk import walk


# Functions
def median(input, filter_width=1):
	ret = walk(input, fun='median', filter_width=filter_width)

	return ret
