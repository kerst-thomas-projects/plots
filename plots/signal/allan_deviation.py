import numpy as np


# Static Functions
def _get_cluster_averages(series: list, cluster_size: int) -> list:
    cluster_averages = []
    
    n_clusters = int(series.__len__() / cluster_size)
    for i in range(n_clusters):
        cluster = series[i * cluster_size:(i + 1) * cluster_size]
        cluster_averages.append(np.mean(cluster))

    return cluster_averages

def _get_cluster_differences(cluster_averages: list) -> list:
    cluster_differences = []

    for i in range(cluster_averages.__len__() - 1):
        y1 = cluster_averages[i]
        y2 = cluster_averages[i + 1]
        cluster_differences.append(y2 - y1)

    return cluster_differences

def _get_single_allan_deviation_value(cluster_differences: list) -> float:
    sq_diff = [_cd ** 2.0  for _cd in cluster_differences]

    return (np.mean(sq_diff) / 2.0) ** .5


# Functions
def allan_deviation(x: list, dt: float=1.0) -> list:
    adev = []
    for i in range(int(x.__len__() / 2)):
        cluster_size = i + 1
        clusters_averages = _get_cluster_averages(x, cluster_size)
        if clusters_averages.__len__() < 10:
            break

        cluster_differences = _get_cluster_differences(clusters_averages)
        _adev = _get_single_allan_deviation_value(cluster_differences)
        adev.append(_adev / 255.0)

    adev = [[(i + 1) * dt, adev[i]] for i in range(adev.__len__())]

    return adev