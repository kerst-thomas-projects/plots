def walk(input, **kwargs):
	from numpy import mean
	from numpy import median

	fun = kwargs['fun'] if 'fun' in kwargs.keys() else 'mean'
	filter_width = kwargs['filter_width'] if 'filter_width' in kwargs.keys() else 1

	pad = int((filter_width - 1) / 2)
	len = input.__len__()

	output = []
	for i in range(len):
		if i < pad: continue
		if len - i - 1 < pad: continue
		left = i - pad
		right = i + pad + 1
		output.append(eval(fun + '(' + str(input[left:right]) + ')'))

	return output
