def prop(data):
	import numpy as np

	x = [_d[0] for _d in data]
	y = [_d[1] for _d in data]

	numerator = np.sum([_x * _y for _x, _y in zip(x, y)])
	denominator = np.sum([_x * _x for _x in x])

	return numerator / denominator
