def no_buildup(data, flow, params_init_passed=None, do_check=False, log_path=None):
	# nonlocals
	x = None
	parameters_initial = None
	parameters_optimised = None
	bounds = None
	return_values = None
	quality = None


	# functions
	def get_x():
		nonlocal x

		x = [i for i in range(data.__len__())]

		return

	def init_parameters():
		nonlocal parameters_initial

		if params_init_passed is not None:
			parameters_initial = params_init_passed

			return

		# c_h2o_0, k_g, k_f, magnitude, offset
		parameters_initial = [1e-4, 1e-2, 1e5, 2e11, 50]

		return

	def init_boundaries():
		import numpy as np
		nonlocal bounds

		# c_h2o_0, k_g, k_f, magnitude, offset
		bounds = ([0, 0, 0, 0, -np.inf], [1, np.inf, np.inf, np.inf, np.inf])

		return

	def fit_fun(_x, c_h2o_0, k_g, k_f, magnitude, offset):
		import no_flow as nf

		c_no = [1e-4 for i in range(x.__len__())]
		c_h2o = nf.water_evolution(flow, t=_x, c_h2o_0=c_h2o_0, k_g=k_g, k_a=k_g)
		photons = nf.radioluminescence(c_no=c_no, c_h2o=c_h2o, function='photons_p', k_f=k_f, flow=flow)
		photons = [magnitude * it + offset for it in photons]

		return photons

	def optimise():
		from scipy.optimize import curve_fit

		nonlocal parameters_optimised

		fit_vals = curve_fit(fit_fun, x, data, p0=parameters_initial, bounds=bounds)
		parameters_optimised = fit_vals[0]

		return

	def get_return_values():
		nonlocal return_values

		return_values = fit_fun(x, *parameters_optimised)

		return

	def check():
		import plotting

		if not do_check:
			return

		print(parameters_optimised)

		plt_data = []
		_plt_data = [(_x, _y) for _x, _y in zip(x, data)]
		plt_data.append(_plt_data)
		_plt_data = [(_x, _y) for _x, _y in zip(x, return_values)]
		plt_data.append(_plt_data)
		color=['0', '0.4']
		plotting.xy(plt_data, color=color)

		return

	def get_fit_quality():
		import numpy as np
		nonlocal quality

		ym = np.mean(data)
		ss_tot = np.sum([(_y - ym) ** 2 for _y in data])
		ss_res = np.sum([(_y - _f) ** 2 for _y, _f in zip(data, return_values)])

		quality = 1 - ss_res / ss_tot

		return

	def write_fit_report():
		if log_path is None:
			return

		f = open(log_path, 'w')

		f.write('c_h2o_0 = ' + '{:.4e}'.format(parameters_optimised[0]) + '\n')
		f.write('k_g = ' + '{:.4e}'.format(parameters_optimised[1]) + '\n')
		f.write('k_f = ' + '{:.4e}'.format(parameters_optimised[2]) + '\n')
		f.write('magnitude = ' + '{:.4e}'.format(parameters_optimised[3]) + '\n')
		f.write('offset = ' + '{:.4e}'.format(parameters_optimised[4]) + '\n')
		f.write('r^2 = ' + str(quality) + '\n')

		f.close()

		return


	# main
	get_x()
	init_parameters()
	init_boundaries()
	optimise()
	get_return_values()
	get_fit_quality()
	write_fit_report()
	check()

	return return_values
