def get_chi_squared(vals, func, ddof=0):
	from scipy.stats import chisquare

	f_obs = [_v[1] for _v in vals]
	f_exp = [func(_v[0]) for _v in vals]

	p_val = chisquare(f_obs=f_obs, f_exp=f_exp, ddof=ddof)[1]

	return p_val
