def no_radioluminescence(data, alpha=1e-6):
	# functions
	def fetch_data():
		_no = [it[0] for it in data]
		_concentrations = [it[1] for it in data]

		return _no, _concentrations

	def init_parameters():
		import constants as c

		return [1e19, c.k_et * 1e2, c.k_QNO]

	def init_boundaries():
		return [0, 1e30]

	def get_no_a(_no, k_et_fit, k_QNO_fit):
		import constants as c
		import numpy as np

		_a = - k_et_fit * k_QNO_fit
		_b = k_QNO_fit * (alpha + c.k_N2 + k_et_fit * _no) + k_et_fit * (c.k_NO + c.k_QN2 * (1 - _no) + k_QNO_fit * _no)
		_c = alpha * (1 - _no) * (c.k_QN2 - k_et_fit) - (alpha + c.k_N2 + k_et_fit * _no) * (c.k_NO + c.k_QN2 * (1 - _no) + k_QNO_fit * _no)
		_d = alpha * k_et_fit * _no * (1 - _no)

		return np.roots([_a, _b, _c, _d])[2]

	def get_nr_of_photons_fit(_no, no_a, amp, k_et_fit, k_QNO_fit):
		import constants as c

		n2_a = alpha * (1 - _no) / (alpha + c.k_N2 + k_et_fit * (_no - no_a))
		denominator = c.k_NO + c.k_QN2 * (1 - _no - n2_a) + k_QNO_fit * (_no - no_a)

		return amp * c.k_NO / denominator * no_a

	def fit_fun(_no, amp, k_et_fit, k_QNO_fit):
		pred = []
		for _n in _no:
			no_a = get_no_a(_n, k_et_fit, k_QNO_fit)
			pred.append(get_nr_of_photons_fit(_n, no_a, amp, k_et_fit, k_QNO_fit))

		return pred

	def optimize():
		from scipy.optimize import curve_fit

		return curve_fit(fit_fun, no, photons, p0=p0, bounds=boundaries)

	def get_deviation():
		import sys

		sys.stdout.write('Fit results:\n')
		for i, it in enumerate(zip(p0, popt)):
			sys.stdout.write('Variable ' + '{:}'.format(i) + ': ')
			sys.stdout.write('{:.2e}'.format(it[0]) + ' -> ')
			sys.stdout.write('{:.2e}'.format(it[1]) + '\n')

		return

	def get_fit_plot():
		import numpy as np

		span = no[-1] - no[0]
		x = np.linspace(no[0] - .6 * span, no[-1] + .6 * span, 1e4)
		y = fit_fun(x, *popt)

		return [[i, j] for i, j in zip(x, y)]


	# main
	no, photons = fetch_data()
	p0 = init_parameters()
	boundaries = init_boundaries()
	popt, pcov = optimize()
	get_deviation()

	return get_fit_plot()
