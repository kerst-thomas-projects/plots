def cerenkov(data, ri_path, x_plot=None):
	# nonlocals
	x = []
	y = []
	ri = None
	parameters_initial = None
	parameters_optimised = None
	bounds = None


	# functions
	def arrange_data():
		for it in data:
			x.append(it[0])
			y.append(it[1])

		return

	def assign_refractive_indicees():
		nonlocal ri

		ri = []

		# read out indicees
		indicees = []
		with open(ri_path, 'r') as f:
			skip_first_line = True
			for line in f:
				if skip_first_line:
					skip_first_line = False

					continue

				comma_pos = line.find(',')
				wl = float(line[:comma_pos]) * 1e3
				index = float(line[comma_pos + 1:-1])
				indicees.append((wl, index))

		# match them to the wavelengths by interpolation
		for _x in x:
			pos = -1
			for i in range(indicees.__len__()):
				if _x < indicees[i][0]:
					pos = i - 1

					break

			mixing_ratio = (_x - indicees[pos][0]) / (indicees[pos + 1][0] - indicees[pos][0])
			_ri = indicees[pos][1] + mixing_ratio * (indicees[pos + 1][1] - indicees[pos][1])
			ri.append(_ri)

		return

	def init_parameters():
		nonlocal parameters_initial

		parameters_initial = [.8, 1e7, 6]

		return

	def init_boundaries():
		import numpy as np

		nonlocal bounds

		bounds = ([.5, -np.inf, -np.inf], [1, np.inf, np.inf])

		return

	def fit_fun(_x, beta, magnitude, offset):
		pred = []
		for __x, _ri in zip(_x, ri):
			beta_n = beta * _ri
			if beta_n < 1:
				pred.append(offset)

				continue

			value_in_brackets = 1 - 1 / (beta_n ** 2)
			intensity = magnitude * value_in_brackets / (__x ** 2) + offset
			pred.append(intensity)

		return pred

	def optimise():
		from scipy.optimize import curve_fit
		nonlocal parameters_optimised

		fit_vals = curve_fit(fit_fun, x, y, p0=parameters_initial, bounds=bounds)
		parameters_optimised = fit_vals[0]

		return

	def get_return_values():
		nonlocal x

		if x_plot is not None:
			x = x_plot
			assign_refractive_indicees()


		ret = fit_fun(x, *parameters_optimised)

		return ret


	# main
	arrange_data()
	assign_refractive_indicees()
	init_parameters()
	init_boundaries()
	optimise()

	return get_return_values()
