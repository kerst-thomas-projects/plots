def lin(data):
	import numpy as np

	x = [it[0] for it in data]
	y = [it[1] for it in data]

	# means
	xm = np.mean(x)
	ym = np.mean(y)

	# covariances
	sxy = sum([(i - xm) * (j - ym) for i, j in zip(x, y)])
	sxx = sum([(i - xm) ** 2 for i in x])

	# fit parameters
	a = sxy / sxx
	b = ym - a * xm

	return a, b
