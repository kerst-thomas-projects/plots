def get_r2(data, fun):
	import numpy as np

	y = [_d[1] for _d in data]
	f = [fun(_d[0]) for _d in data]

	ym = np.mean(y)
	ss_res = np.sum([(_y - _f) ** 2 for _y, _f in zip(y, f)])
	ss_tot = np.sum([(_y - ym) ** 2 for _y in y])
	r2 = 1 - ss_res / ss_tot

	return r2
