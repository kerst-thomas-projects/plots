def no_jump(data, flows, boundaries=None, do_check=None, log_path=None, constants=None, **p0):
	# params_ordered
	params_ordered = [['no', False], ['t_stop_1', False], ['c_h2o_n2', False], ['c_h2o_0', False], ['k_g', False], ['k_a', False], ['k_phi', False], ['magnitude', False], ['offset', False]]


	# nonlocals
	bounds = None
	params_initial = None
	params_optimised = None
	return_values = None
	mode = None
	fit_iteration = 1


	# functions
	def get_params_initial():
		nonlocal params_ordered
		nonlocal params_initial

		params_initial = []
		for i, _p in enumerate(params_ordered):
			if _p[0] in p0.keys():
				_p[1] = True
				params_initial.append(p0[_p[0]])

		# magnitude
		if not params_ordered[-2][1]:
			params_ordered[-2][1] = True
			params_initial.append(1)

		# offset
		if not params_ordered[-1][1]:
			params_ordered[-1][1] = True
			params_initial.append(0)


		return

	def init_boundaries():
		import numpy as np
		nonlocal bounds

		if boundaries is not None:
			bounds = boundaries

			return

		bounds_lower = []
		bounds_upper = []

		bounds_defaults = {
			'no': [0, 1],
			't_stop_1': [0, 10],
			'c_h2o_n2': [0, .01],
			'c_h2o_0': [0, 1],
			'k_g': [0, np.inf],
			'k_a': [0, np.inf],
			'k_phi': [0, np.inf],
			'magnitude': [0, np.inf],
			'offset': [-np.inf, np.inf],
		}

		for i, _p in enumerate(params_ordered):
			if not _p[1]:
				continue

			bounds_lower.append(bounds_defaults[_p[0]][0])
			bounds_upper.append(bounds_defaults[_p[0]][1])

		bounds = (bounds_lower, bounds_upper)

		return

	def extend():
		# nonlocals
		x_extended = None
		y_extended = None


		# functions
		def extend_x():
			nonlocal x_extended

			t_start = constants['t_start']
			t_end = constants['t_end']

			x_extended = []
			for _d in data:
				_x = [t_start + i for i in range(t_end - t_start)]
				x_extended.extend(_x)

			return

		def extend_y():
			nonlocal y_extended

			t_start = constants['t_start']
			t_end = constants['t_end']

			y_extended = []
			for _d in data:
				_y = _d[t_start:t_end]
				y_extended.extend(_y)

			return


		# main
		extend_x()
		extend_y()

		return x_extended, y_extended

	def compress(x, y):
		l = flows.__len__()
		item_len = int(x.__len__() / l)

		ret = []
		for i in range(l):
			offset = i * item_len
			ret_item = [[x[offset + j], y[offset + j]] for j in range(item_len)]
			ret.append(ret_item)

		return ret

	def fit_fun(x, *kwargs):
		import no_flow as nf
		import sys
		import system
		nonlocal fit_iteration

		params = {}
		cnt = 0
		for i, _p in enumerate(params_ordered):
			if not _p[1]:
				continue

			params = {**params, _p[0]:kwargs[cnt]}
			cnt += 1

		display = None
		if mode == 'fitting':
			display = '\rIteration ' + str(fit_iteration) + ' - '

		if mode == 'init':
			display = '\rInitialising.. '

		if mode == 'test':
			display = '\rTesting.. '

		y_stacked = nf.jump(flows=flows, **params, **constants, display=display)
		y = []
		[y.extend(_y) for _y in y_stacked]

		if mode is not None:
			system.erase_last_n_chars(10)

		if mode == 'fitting':
			fit_iteration += 1

		magnitude = params['magnitude']
		offset = params['offset']

		y = [magnitude * _y + offset for _y in y]

		return y

	def optimise():
		from scipy.optimize import curve_fit
		import sys
		nonlocal params_optimised
		nonlocal return_values
		nonlocal mode

		mode = 'fitting'
		x, y = extend()
		fit_vals = curve_fit(fit_fun, x, y, p0=params_initial, bounds=bounds, method='dogbox')
		params_optimised = fit_vals[0]
		mode = None
		sys.stdout.write('\n')

		return_values = fit_fun(x, *params_optimised)

		return

	def write_fit_report():
		# functions
		def get_fit_quality():
			import numpy as np

			x, y = extend()
			ym = np.mean(y)
			ss_tot = np.sum([(_y - ym) ** 2 for _y in y])
			ss_res = np.sum([(_y - _f) ** 2 for _y, _f in zip(y, return_values)])

			quality = 1 - ss_res / ss_tot

			return quality

		# main
		if log_path is None:
			return

		f = open(log_path, 'w')

		cnt = 0
		for i, _p in enumerate(params_ordered):
			if not _p[1]:
				continue

			f.write(_p[0] + ' = ' + '{:.4e}'.format(params_optimised[cnt]) + '\n')
			cnt += 1

		f.write('r^2 = ' + str(get_fit_quality()) + '\n')

		f.close()

		return

	def check():
		import plotting as p
		import sys
		nonlocal mode

		if do_check is None:
			return

		plt_vals = []

		x, y = extend()
		plt_vals.extend(compress(x, y))

		mode = 'init'
		y = fit_fun(x, *params_initial)
		sys.stdout.write('\n')
		plt_vals.extend(compress(x, y))

		if do_check != 'init':
			mode = 'test'
			y = fit_fun(x, *params_optimised)
			sys.stdout.write('\n')
			plt_vals.extend(compress(x, y))

		color = []
		[color.append(p.black) for i in range(flows.__len__())]
		[color.append(p.blue) for i in range(flows.__len__())]
		[color.append(p.red) for i in range(flows.__len__())]

		p.xy(plt_vals, color=color)

		return


	# main
	get_params_initial()
	if do_check != 'init':
		init_boundaries()
		optimise()
		write_fit_report()

	check()

	if do_check == 'init':
		return

	return compress(extend()[0], return_values)
