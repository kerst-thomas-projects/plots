def fetch_pmt(path):
	f = open(path, 'r')
	vals = [int(line.split(' ')[-1]) for line in f]
	f.close()

	return vals
