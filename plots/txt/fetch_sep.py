def fetch_sep(path, sep):
	with open(path, 'r') as f:
		vals = [line.split(sep) for line in f]

	return vals
