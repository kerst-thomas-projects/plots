def save_to_csv(path, items, header):
	import sys

	if items[0].__len__() != header.__len__():
		sys.stdout.write(
			'SAVE_TO_CSV ERROR: List items and header must have the same length. '
			+ 'List item length: ' + str(items[0].__len__()) + '. '
			+ 'Header length: ' + str(header.__len__()) + '.\n'
			)

		return

	f = open(path, 'w')
	# header
	for _h in header:
		f.write(_h)
		f.write(', ')

	f.write('\n')

	for _i in items:
		for _ii in _i:
			f.write(str(_ii))
			f.write(', ')

		f.write('\n')

	f.write('\n')

	f.close()

	return
