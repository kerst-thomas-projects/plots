def fetch_new_line_sep_vals(path):
	with open(path, 'r') as f:
		vals = [float(line) for line in f]

	return vals
