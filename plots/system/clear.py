def clear():
	import os
	import sys

	if sys.platform == 'darwin': os.system("printf '\e[2J\e[3J\e[H'")
	if sys.platform == 'win32': os.system("cls")
	if sys.platform == 'linux': os.system("printf '\x1b[2J\x1b[3J\x1b[H'")

	return
