def get_four_bytes_specificer():
	import sys

	if sys.platform == 'win32': return 'L'
	if sys.platform == 'darwin': return 'I'
	if sys.platform == 'linux': return 'I'

	return
