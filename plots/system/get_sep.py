def get_sep():
	import sys

	sep = '/' if sys.platform == 'darwin' or sys.platform == 'linux' else '\\'

	return sep
