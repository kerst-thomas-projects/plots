def erase_last_n_chars(n):
	import os
	import sys

	if sys.platform == 'darwin':
		os.system("printf '\e[" + str(n) + "D\e[0K'")

	if sys.platform == 'win32':
		pass  # No ANSI support

	return
