def erase_line():
	import os
	import sys

	if sys.platform == 'darwin':
		os.system("printf '\e[1J'")

	if sys.platform == 'win32':
		pass  # No ANSI support

	return
