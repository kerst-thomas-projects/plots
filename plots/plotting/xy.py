from .xy_lib.__init__ import *
from typing import Any


# Global Variables
params = None


# Static Functions
def _set_params(**kwargs) -> None:
    global params
    
    params = kwargs

    return

def _p(key: str) -> Any:
    if key in params:
        return params[key]

    return None

def _plot_data(x: Any, y: Any, yerr: Any) -> list[Any]:
    kwargs = {
        'x': x,
        'y': y,
        'yerr': yerr,
        'color': _p('color'),
        'linestyle': _p('linestyle'),
        'linewidth': _p('linewidth'),
        'marker': _p('marker'),
        'markersize': _p('markersize'),
        'elinewidth': _p('elinewidth'),
        'capsize': _p('capsize'),
    }

    if type(x[0]) is list:
        handles = plot_multiple(**kwargs)
    else:
        kwargs = {**kwargs, 'errmode': _p('errmode')}
        handles = plot_single(**kwargs)

    return handles

def _modify_axes(x: Any, y: Any) -> None:
    kwargs = {
        'borderwidth': _p('borderwidth'),
        'xlabpos': _p('xlabpos'),
        'ylabpos': _p('ylabpos'),
        'fontsize': _p('fontsize'),
        'xscale': _p('xscale'),
        'yscale': _p('yscale'),
    }
    set_axes(**kwargs)

    set_ticks_x(_p('xticks'), _p('use_minor_ticks_x'), _p('xtickslab'))
    set_limits_x(x, _p('xlim'))
    set_label_x(_p('xlab'), _p('xlabcol'), _p('fontsize'))

    set_ticks_y(_p('yticks'), _p('use_minor_ticks_y'), _p('ytickslab'))
    set_limits_y(y, _p('ylim'))
    set_label_y(_p('ylab'), _p('ylabcol'), _p('fontsize'))

    return

def _add_twinx(twinx: Any) -> None:
    if twinx is None:
        return
    
    x, y, _ = get_plot_data(twinx)

    add_shade_twinx(_p('shade_twinx'))
    add_patch_twinx(_p('patch_twinx'))

    kwargs = {
        'x': x,
        'y': y,
        'color_twinx': _p('color_twinx'),
        'linestyle_twinx': _p('linestyle_twinx'),
        'linewidth_twinx': _p('linewidth_twinx'),
        'marker_twinx': _p('marker_twinx'),
        'markersize_twinx': _p('markersize_twinx'),
    }
    if type(x[0]) is list:
        plot_multiple_twinx(**kwargs)
    else:
        plot_single_twinx(**kwargs)

    set_axes_twinx(_p('ytwinxlabpos'), _p('fontsize'), _p('color_twinx_ax'))
    set_ticks_y_twinx(_p('ytwinxticks'), _p('use_minor_ticks_ytwinx'),
                      _p('ytwinxtickslab'))
    set_limits_ytwinx(y, _p('ytwinxlim'))
    set_label_ytwinx(_p('ytwinxlab'), _p('ytwinxlabcol'), _p('fontsize'))

    return


# Functions
def xy(data, twinx=None, **kwargs):
    _set_params(**kwargs)
    x, y, yerr = get_plot_data(data)
    init_plot()

    set_font(_p('font'))
    add_shade(_p('shade'))
    add_patch(_p('patch'))
    handles = _plot_data(x, y, yerr)
    _modify_axes(x, y)
    add_legend(_p('legend'), handles)
    add_arrow(_p('arrow'))
    add_text(_p('text'))
    add_figimage(_p('figimage'))
    _add_twinx(twinx)

    save_plot(_p('save_path'))

    return
