def heat_map_with_overlay(data, overlays, save_path=None, vmin=None, vmax=None, use_colorbar=False):
	import matplotlib as mpl
	from matplotlib import pyplot as plt
	from matplotlib import patches as patches
	from matplotlib import cm as cm

	mpl.rc('font', family='Calibri', size=17)

	fig = plt.figure()
	plt.imshow(data, cmap=cm.jet, vmin=vmin, vmax=vmax)
	ax = plt.gca()
	ax.axis('off')

	for it in overlays:
		patch = patches.Rectangle(*it, linewidth=2, edgecolor=[1.0, 1.0, 1.0, 1.0] ,facecolor=[0.0, 0.0, 0.0, 0.0])
		ax.add_patch(patch)

	if use_colorbar: plt.colorbar()

	if save_path is None: plt.show()
	else: plt.savefig(save_path, bbox_inches='tight', transparent=True)

	plt.close(fig)

	return
