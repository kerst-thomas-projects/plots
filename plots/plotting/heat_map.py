def heat_map(data, save_path=None, vmin=None, vmax=None, use_colorbar=False):
	import matplotlib as mpl
	from matplotlib import pyplot as plt
	from matplotlib import cm as cm

	mpl.rc('font', family='Calibri', size=17)

	fig = plt.figure()
	plt.imshow(data, cmap=cm.jet, vmin=vmin, vmax=vmax)
	ax = plt.gca()
	ax.axis('off')
	ax.set_ylabel('hi')

	if use_colorbar: plt.colorbar()

	if save_path is None: plt.show()
	else: plt.savefig(save_path, bbox_inches='tight', transparent=True)

	plt.close(fig)

	return
