import matplotlib.pyplot as plt
from .constants import *
from typing import Any


# Static Functions
def _get_lim_single(x: list[float]):
    return [min(x), max(x)]

def _get_lim_multiple(x: list[list[float]]):
    mins = [min(it) for it in x]
    maxs = [max(it) for it in x]

    return [min(mins), max(maxs)]


# Functions
def set_limits_x(x: Any, xlim: tuple[float]) -> None:
    xl = DEFAULT_XLIM if xlim is None else xlim

    if xl is None:
        if type(x[0]) is list:
            xl = _get_lim_multiple(x)
        else:
            xl = _get_lim_single(x)

    ax = plt.gca()
    ax.set_xlim(xl)

    return
