import matplotlib.pyplot as plt
from .constants import *


# Functions
def set_label_y(ylab: str, ylabcol: tuple[float], fontsize: float) -> None:
    yl = DEFAULT_YLAB if ylab is None else ylab
    ylc = DEFAULT_YLABCOL if ylabcol is None else ylabcol
    fs = DEFAULT_FONTSIZE if fontsize is None else fontsize

    plt.ylabel(yl, color=ylc, fontsize=fs)

    return
