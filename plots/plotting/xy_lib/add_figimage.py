import matplotlib.pyplot as plt
from .constants import *


# Functions
def add_figimage(figimage: list[dict]) -> None:
    f = DEFAULT_FIGIMAGE if figimage is None else figimage
    
    if f is None:
        return

    ax = plt.gca()
    for _f in f:
        ax.figure.figimage(**_f)

    return
