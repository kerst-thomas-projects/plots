import sys


# Constants
_TYPES = [list, tuple]


# Static Functions
def _check_data_decodability(data) -> None:
    if type(data) in _TYPES:
        return

    wrt_str = [
        'Cannot decode the plot data.',
        ' Aborting program executing.',
        '\n',
    ]
    sys.stdout.write(''.join(wrt_str))
    sys.exit(1)

    return

def _get_values(data: list[float]) -> tuple[list[float]]:
    x = [i for i in range(data.__len__())]
    y = [it for it in data]

    return x, y

def _get_values_from_list(data: list[list[float]]) -> tuple[list[float]]:
    x = [it[0] for it in data]
    y = [it[1] for it in data]
    yerr = None

    if data[0].__len__() >= 3:
        yerr = [it[2] for it in data]

    return x, y, yerr

def _read_multi(data: list[list[list[float]]]) -> tuple[list[list[float]]]:
    x = []
    y = []
    yerr = []

    for _d in data:
        _x = [it[0] for it in _d]
        _y = [it[1] for it in _d]
        x.append(_x)
        y.append(_y)

        if _d[0].__len__() < 3:
            yerr.append(None)
            continue

        _yerr = [it[2] for it in _d]
        yerr.append(_yerr)

    return x, y, yerr


# Functions
def get_plot_data(data):
    _check_data_decodability(data)

    if type(data[0]) not in _TYPES:
        x, y = _get_values(data)
        yerr = None
    elif type(data[0][0]) not in _TYPES:
        x, y, yerr = _get_values_from_list(data)
    else:
        x, y, yerr = _read_multi(data)

    return x, y, yerr
