import matplotlib.pyplot as plt
from .constants import *
from .colours import *
from typing import Any


# Functions
def plot_single_twinx(x: list[float], y: list[float], color_twinx: Any,
                      linestyle_twinx: str, linewidth_twinx: float,
                      marker_twinx: str, markersize_twinx: float) -> None:
    
    c = DEFAULT_COLOR_TWINX if color_twinx is None else color_twinx
    ls = DEFAULT_LINESTYLE_TWINX \
        if linestyle_twinx is None else linestyle_twinx
    lw = DEFAULT_LINEWIDTH_TWINX \
        if linewidth_twinx is None else linewidth_twinx
    m = DEFAULT_MARKER_TWINX if marker_twinx is None else marker_twinx
    ms = DEFAULT_MARKERSIZE_TWINX \
        if markersize_twinx is None else markersize_twinx
    
    kwargs = {
        'color': c,
        'linestyle': ls,
        'linewidth': lw,
        'marker': m,
        'markersize': ms,
    }
    ax = plt.gca().twinx()
    ax.plot(x, y, **kwargs)

    return
