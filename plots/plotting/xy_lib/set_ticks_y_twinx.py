import matplotlib.pyplot as plt
from .constants import *


# Static Functions
def _set_major_ticks(yt: list[float]) -> None:
    if yt is None:
        return

    ax = plt.gca()
    ax.set_yticks(yt)
    
    return

def _set_minor_ticks(yt: list[float], umt: int) -> None:
    if umt is None:
        return
    
    minor_ticks = []
    for i in range(yt.__len__() - 1):
        neyt = yt[i + 1]
        prev = yt[i]
        width_step = (neyt - prev) / umt
        for j in range(umt):
            minor_ticks.append(prev + j * width_step)

    ax = plt.gca()
    ax.set_yticks(minor_ticks, minor=True)
    ax.set_yticklabels([], minor=True)
    
    return

def _set_tick_labels(ytl: str) -> None:
    if ytl is None:
        return

    ax = plt.gca()
    # locs = ax.get_yticks()[0]
    ax.set_yticklabels(ytl)
    
    return


# Functions
def set_ticks_y_twinx(ytwinxticks: list[float], use_minor_ticks_ytwinx: int,
                      ytwinxtickslab: str) -> None:
    
    yt = DEFAULT_YTWINXTICKS if ytwinxticks is None else ytwinxticks
    umt = DEFAULT_USE_MINOR_TICKS_YTWINX \
        if use_minor_ticks_ytwinx is None else use_minor_ticks_ytwinx
    ytl = DEFAULT_YTWINXTICKSLAB if ytwinxtickslab is None else ytwinxtickslab

    _set_major_ticks(yt)
    _set_minor_ticks(yt, umt)
    _set_tick_labels(ytl)

    return
