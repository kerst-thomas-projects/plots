import matplotlib.pyplot as plt
from .constants import *


# Functions
def add_arrow(arrow: list[dict]) -> None:
    a = DEFAULT_ARROW if arrow is None else arrow
    
    if a is None:
        return

    for _a in a:
        plt.arrow(**_a)

    return
