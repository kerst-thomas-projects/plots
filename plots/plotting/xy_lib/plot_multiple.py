import matplotlib.pyplot as plt
from .constants import *
from typing import Any
import numpy as np


# Static Functions
def _expand_modifier(tar: Any, x: list[list[float]]):
    if type(tar) is list:
        return tar

    expanded_tar = [tar for i in range(x.__len__())]

    return expanded_tar


# Functions
def plot_multiple(x: list[list[float]], y: list[list[float]],
                  yerr: list[list[float]], color: Any, linestyle: str,
                  linewidth: float, marker: str, markersize: float,
                  elinewidth: float, capsize: float) -> list[Any]:
    
    c = DEFAULT_COLOR if color is None else color
    ls = DEFAULT_LINESTYLE if linestyle is None else linestyle
    lw = DEFAULT_LINEWIDTH if linewidth is None else linewidth
    m = DEFAULT_MARKER if marker is None else marker
    ms = DEFAULT_MARKERSIZE if markersize is None else markersize
    e = DEFAULT_ELINEWIDTH if elinewidth is None else elinewidth
    cs = DEFAULT_CAPSIZE if capsize is None else capsize

    c = _expand_modifier(c, x)
    ls = _expand_modifier(ls, x)
    lw = _expand_modifier(lw, x)
    m = _expand_modifier(m, x)
    ms = _expand_modifier(ms, x)
    e = _expand_modifier(e, x)
    cs = _expand_modifier(cs, x)

    handles = []
    for i in range(x.__len__()):
        kwargs = {
            'color': c[i],
            'linestyle': ls[i],
            'linewidth': lw[i],
            'marker': m[i],
            'markersize': ms[i],
        }
        if yerr[i] is None:
            curve, = plt.plot(x[i], y[i], **kwargs)
        else:
            kwargs = {**kwargs, 'elinewidth': e[i], 'capsize': cs[i]}
            curve, = plt.errorbar(x[i], y[i], yerr=yerr[i], **kwargs)

        handles.append(curve)

    return handles
