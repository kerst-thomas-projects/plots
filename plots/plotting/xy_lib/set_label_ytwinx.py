import matplotlib.pyplot as plt
from .constants import *


# Functions
def set_label_ytwinx(ytwinxlab: str, ytwinxlabcol: tuple[float],
                     fontsize: float) -> None:
    yl = DEFAULT_YTWINXLAB if ytwinxlab is None else ytwinxlab
    ylc = DEFAULT_YTWINXLABCOL if ytwinxlabcol is None else ytwinxlabcol
    fs = DEFAULT_FONTSIZE if fontsize is None else fontsize

    ax = plt.gca()
    ax.set_ylabel(yl, color=ylc, fontsize=fs)

    return
