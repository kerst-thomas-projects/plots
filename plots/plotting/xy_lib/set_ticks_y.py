import matplotlib.pyplot as plt
from .constants import *


# Static Functions
def _set_major_ticks(yt: list[float]) -> None:
    if yt is None:
        return

    ax = plt.gca()
    ax.set_yticks(yt)
    
    return

def _set_minor_ticks(yt: list[float], umt: int) -> None:
    if umt is None:
        return
    
    minor_ticks = []
    for i in range(yt.__len__() - 1):
        neyt = yt[i + 1]
        prev = yt[i]
        width_step = (neyt - prev) / umt
        for j in range(umt):
            minor_ticks.append(prev + j * width_step)

    ax = plt.gca()
    ax.set_yticks(minor_ticks, minor=True)
    ax.set_yticklabels([], minor=True)
    
    return

def _set_tick_labels(ytl: str) -> None:
    if ytl is None:
        return

    locs = plt.yticks()[0]
    plt.yticks(locs, ytl)
    
    return


# Functions
def set_ticks_y(yticks: list[float], use_minor_ticks_y: int,
                ytickslab: str) -> None:
    
    yt = DEFAULT_YTICKS if yticks is None else yticks
    umt = DEFAULT_USE_MINOR_TICKS_Y \
        if use_minor_ticks_y is None else use_minor_ticks_y
    ytl = DEFAULT_YTICKSLAB if ytickslab is None else ytickslab

    _set_major_ticks(yt)
    _set_minor_ticks(yt, umt)
    _set_tick_labels(ytl)

    return
