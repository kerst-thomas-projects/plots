import matplotlib.pyplot as plt
from .constants import *
from typing import Any


# Static Functions
def _get_lim_single(y: list[float]):
    return [min(y), max(y)]

def _get_lim_multiple(y: list[list[float]]):
    mins = [min(it) for it in y]
    maxs = [max(it) for it in y]

    return [min(mins), max(maxs)]


# Functions
def set_limits_ytwinx(y: Any, ytwinxlim: tuple[float]) -> None:
    yl = DEFAULT_YTWINXLIM if ytwinxlim is None else ytwinxlim

    if yl is None:
        if type(y[0]) is list:
            yl = _get_lim_multiple(y)
        else:
            yl = _get_lim_single(y)

    ax = plt.gca()
    ax.set_ylim(yl)

    return
