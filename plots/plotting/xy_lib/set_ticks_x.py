import matplotlib.pyplot as plt
from .constants import *


# Static Functions
def _set_major_ticks(xt: list[float]) -> None:
    if xt is None:
        return

    ax = plt.gca()
    ax.set_xticks(xt)
    
    return

def _set_minor_ticks(xt: list[float], umt: int) -> None:
    if umt is None:
        return
    
    minor_ticks = []
    for i in range(xt.__len__() - 1):
        next = xt[i + 1]
        prev = xt[i]
        width_step = (next - prev) / umt
        for j in range(umt):
            minor_ticks.append(prev + j * width_step)

    ax = plt.gca()
    ax.set_xticks(minor_ticks, minor=True)
    ax.set_xticklabels([], minor=True)
    
    return

def _set_tick_labels(xtl: str) -> None:
    if xtl is None:
        return

    locs = plt.xticks()[0]
    plt.xticks(locs, xtl)
    
    return


# Functions
def set_ticks_x(xticks: list[float], use_minor_ticks_x: int,
                xtickslab: str) -> None:
    
    xt = DEFAULT_XTICKS if xticks is None else xticks
    umt = DEFAULT_USE_MINOR_TICKS_X \
        if use_minor_ticks_x is None else use_minor_ticks_x
    xtl = DEFAULT_XTICKSLAB if xtickslab is None else xtickslab

    _set_major_ticks(xt)
    _set_minor_ticks(xt, umt)
    _set_tick_labels(xtl)

    return
