import matplotlib.pyplot as plt
from .constants import *


# Functions
def set_axes_twinx(ytwinxlabpos: tuple[float], fontsize: float,
                   color_twinx_ax: tuple[float]) -> None:
    
    yl = DEFAULT_YTWINXLABPOS if ytwinxlabpos is None else ytwinxlabpos
    fs = DEFAULT_FONTSIZE if fontsize is None else fontsize
    ca = DEFAULT_COLOR_TWINX_AX if color_twinx_ax is None else color_twinx_ax
    
    ax = plt.gca()
    ax.yaxis.set_label_coords(*yl)
    plt.yticks(fontsize=fs, color=ca)
    ax.get_yaxis().set_tick_params(which='both', direction='in', width=1.5,
                                   color=ca)
    ax.get_yaxis().set_tick_params(which='major', length=7)
    ax.get_yaxis().set_tick_params(which='minor', length=4)
    
    return
