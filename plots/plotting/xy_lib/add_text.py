import matplotlib.pyplot as plt
from .constants import *


# Functions
def add_text(text: list[dict]) -> None:
    t = DEFAULT_TEXT if text is None else text
    
    if text is None:
        return

    for _t in t:
        plt.text(**_t)

    return
