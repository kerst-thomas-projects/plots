import matplotlib.pyplot as plt
from .constants import *
from .colours import *
from typing import Any


# Functions
def plot_single(x: list[float], y: list[float], yerr: list[float],
                color: Any, linestyle: str, linewidth: float, marker: str,
                markersize: float, elinewidth: float, capsize: float,
                errmode: str) -> None:
    
    c = DEFAULT_COLOR if color is None else color
    ls = DEFAULT_LINESTYLE if linestyle is None else linestyle
    lw = DEFAULT_LINEWIDTH if linewidth is None else linewidth
    m = DEFAULT_MARKER if marker is None else marker
    ms = DEFAULT_MARKERSIZE if markersize is None else markersize
    el = DEFAULT_ELINEWIDTH if elinewidth is None else elinewidth
    cs = DEFAULT_CAPSIZE if capsize is None else capsize
    em = DEFAULT_ERRMODE if errmode is None else errmode
    
    kwargs = {
        'color': c,
        'linestyle': ls,
        'linewidth': lw,
        'marker': m,
        'markersize': ms,
    }

    if yerr is None:
        plt.plot(x, y, **kwargs)
    elif em == 'errorbar':
        plt.errorbar(x, y, yerr=yerr, elinewidth=el, capsize=cs, **kwargs)
    elif em == 'shade':
        plt.plot(x, y, **kwargs)
        y1 = [_y - _ye for _y, _ye in zip(y, yerr)]
        y2 = [_y + _ye for _y, _ye in zip(y, yerr)]
        plt.fill_between(x, y1=y1, y2=y2, edgecolor=grey30, facecolor=grey30)

    return
