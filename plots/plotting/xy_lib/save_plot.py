import matplotlib.pyplot as plt
from .constants import *


# Functions
def save_plot(save_path: str) -> None:
    p = DEFAULT_SAVE_PATH if save_path is None else save_path

    if p is None:
        plt.show()

        return

    plt.savefig(p, bbox_inches='tight', dpi=400)
    fig = plt.gcf()
    plt.close(fig)

    return
