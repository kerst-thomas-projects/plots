import matplotlib.pyplot as plt
from .constants import *


# Functions
def set_axes(borderwidth: float, xlabpos: tuple[float],
             ylabpos: tuple[float], fontsize: float, xscale: str,
             yscale: str) -> None:
    
    bw = DEFAULT_BORDERWIDTH if borderwidth is None else borderwidth
    xlp = DEFAULT_XLABPOS if xlabpos is None else xlabpos
    ylp = DEFAULT_YLABPOS if ylabpos is None else ylabpos
    fs = DEFAULT_FONTSIZE if fontsize is None else fontsize
    xs = DEFAULT_XSCALE if xscale is None else xscale
    ys = DEFAULT_YSCALE if yscale is None else yscale

    ax = plt.gca()
    [ax.spines[it].set_linewidth(bw) for it in ax.spines]
    ax.xaxis.set_label_coords(*xlp)
    ax.yaxis.set_label_coords(*ylp)
    ax.get_xaxis().set_ticks_position('both')
    ax.get_yaxis().set_ticks_position('both')
    plt.xticks(fontsize=fs)
    plt.yticks(fontsize=fs)
    ax.get_xaxis().set_tick_params(which='both', direction='in', width=1.5)
    ax.get_yaxis().set_tick_params(which='both', direction='in', width=1.5)
    ax.get_xaxis().set_tick_params(which='major', length=7)
    ax.get_yaxis().set_tick_params(which='major', length=7)
    ax.get_xaxis().set_tick_params(which='minor', length=4)
    ax.get_yaxis().set_tick_params(which='minor', length=4)
    ax.set_xscale(xs)
    ax.set_yscale(ys)
    
    return
