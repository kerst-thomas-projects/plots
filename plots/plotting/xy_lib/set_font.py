import matplotlib.font_manager as fm
import matplotlib as mpl
from .constants import *

# Functions
def set_font(font: str) -> None:
    f = DEFAULT_FONT if font is None else font

    # Only add fonts ONCE instead of rescanning every time
    if not hasattr(set_font, "_fonts_loaded"):
        font_dirs = ['/Library/Fonts/']
        font_files = fm.findSystemFonts(fontpaths=font_dirs)
        for font_file in font_files:
            fm.fontManager.addfont(font_file)
        
        # Mark that fonts are already loaded
        set_font._fonts_loaded = True

    # Set the font without triggering a rescan
    mpl.rc('font', family=f, size=17)

    return
