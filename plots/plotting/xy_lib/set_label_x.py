import matplotlib.pyplot as plt
from .constants import *


# Functions
def set_label_x(xlab: str, xlabcol: tuple[float], fontsize: float) -> None:
    xl = DEFAULT_XLAB if xlab is None else xlab
    xlc = DEFAULT_XLABCOL if xlabcol is None else xlabcol
    fs = DEFAULT_FONTSIZE if fontsize is None else fontsize

    plt.xlabel(xl, color=xlc, fontsize=fs)

    return
