import matplotlib.pyplot as plt
from .constants import *


# Functions
def add_shade(shade: list[dict]) -> None:
    s = DEFAULT_SHADE if shade is None else shade
    
    if s is None:
        return

    for _s in s:
        plt.fill_between(**_s)

    return
