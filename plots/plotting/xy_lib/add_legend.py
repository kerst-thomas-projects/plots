import matplotlib.pyplot as plt
from .constants import *
from typing import Any


# Functions
def add_legend(legend: dict, handles: Any) -> None:
    l = DEFAULT_LEGEND if legend is None else legend
    
    if l is None:
        return

    if handles is None:
        plt.legend(**legend)
    else:
        plt.legend(handles=handles, **legend)

    return
