import matplotlib.pyplot as plt
from .constants import *
from typing import Any


# Static Functions
def _expand_modifier(tar: Any, x: list[list[float]]):
    if type(tar) is list:
        return tar

    expanded_tar = [tar for i in range(x.__len__())]

    return expanded_tar


# Functions
def plot_multiple_twinx(x: list[list[float]], y: list[list[float]],
                        color_twinx: Any, linestyle_twinx: str,
                        linewidth_twinx: float, marker_twinx: str,
                        markersize_twinx: float) -> None:
    
    c = DEFAULT_COLOR_TWINX if color_twinx is None else color_twinx
    ls = DEFAULT_LINESTYLE_TWINX \
        if linestyle_twinx is None else linestyle_twinx
    lw = DEFAULT_LINEWIDTH_TWINX \
        if linewidth_twinx is None else linewidth_twinx
    m = DEFAULT_MARKER_TWINX if marker_twinx is None else marker_twinx
    ms = DEFAULT_MARKERSIZE_TWINX \
        if markersize_twinx is None else markersize_twinx

    c = _expand_modifier(c, x)
    ls = _expand_modifier(ls, x)
    lw = _expand_modifier(lw, x)
    m = _expand_modifier(m, x)
    ms = _expand_modifier(ms, x)

    ax = plt.gca().twinx()
    for i in range(x.__len__()):
        kwargs = {
            'color': c[i],
            'linestyle': ls[i],
            'linewidth': lw[i],
            'marker': m[i],
            'markersize': ms[i],
        }
        ax.plot(x[i], y[i], **kwargs)

    return
