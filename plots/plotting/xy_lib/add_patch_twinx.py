import matplotlib.pyplot as plt
import matplotlib as mpl
from .constants import *


# Functions
def add_patch_twinx(patch: list[dict]) -> None:
    p = DEFAULT_PATCH_TWINX if patch is None else patch
    
    if p is None:
        return

    ax = plt.gca().twinx()
    for _p in p:
        polygon = mpl.patches.Polygon(**_p)
        ax.add_patch(polygon)

    return
