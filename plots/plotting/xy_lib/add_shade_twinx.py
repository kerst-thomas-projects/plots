import matplotlib.pyplot as plt
from .constants import *


# Functions
def add_shade_twinx(shade: list[dict]) -> None:
    s = DEFAULT_SHADE_TWINX if shade is None else shade
    
    if s is None:
        return

    ax = plt.gca().twinx()
    for _s in s:
        ax.fill_between(**_s)

    return
