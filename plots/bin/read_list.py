def read_list(path, elem_len):
	import struct

	with open(path, 'rb') as f: bytes = f.read()
	elem_num = int(bytes.__len__() / 4 / elem_len)
	ret_val = [struct.unpack(str(elem_len) + 'f', bytes[4 * i * elem_len: 4 * (i + 1) * elem_len]) for i in range(elem_num)]

	return ret_val
