def write_lists_of_int(path, data):
	import struct
	import system

	f = open(path, 'wb')

	s = system.get_four_bytes_specificer()
	f.write(struct.pack(s, data.__len__()))
	for _data in data:
		f.write(struct.pack(s, _data.__len__()))
		l = str(_data.__len__())
		f.write(struct.pack(l + s, *_data))

	f.close()

	return
