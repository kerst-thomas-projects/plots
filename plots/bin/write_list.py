def write_list(path, data):
	import struct

	if type(data[0]) is list:
		with open(path, 'wb') as f:
			[[f.write(struct.pack('f', item)) for item in list_elem] for list_elem in data]
	else:
		with open(path, 'wb') as f:
			[f.write(struct.pack('f', item)) for item in data]

	return
