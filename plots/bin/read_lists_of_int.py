def read_lists_of_int(path, scaling=1, time_divisor=False):
	import struct
	import system

	lists = []

	s = system.get_four_bytes_specificer()
	f = open(path, 'rb')

	n_lists = struct.unpack(s, f.read(4))[0]
	for i in range(n_lists):
		l = struct.unpack(s, f.read(4))[0]
		_list = list(struct.unpack(str(l) + s, f.read(4 * l)))
		_list = [it / scaling for it in _list]
		if time_divisor:
			_list = [(j / time_divisor, _list[j]) for j in range(l)]

		lists.append(_list)

	f.close()

	return lists
