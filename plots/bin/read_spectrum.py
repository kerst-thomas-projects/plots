def read_spectrum(path):
	# nonlocals
	spectrum_raw = None
	n_spectra = 0
	spectrum = None


	# functions
	def read_int(f):
		import struct

		val = int(struct.unpack('d', f.read(8))[0])

		return val

	def read_out_binary():
		import system

		nonlocal spectrum_raw
		nonlocal n_spectra

		s = []
		with open(path, 'rb') as f:
			n_wavelengths = read_int(f)
			for i in range(n_wavelengths):
				wl = read_int(f)
				n_cnts = read_int(f)
				item = [wl, []]
				for i in range(n_cnts): item[1].append(read_int(f))
				s.append(item)

			n_spectra += read_int(f)

		if spectrum_raw is None:
			spectrum_raw = s

			return

		for i in range(spectrum_raw.__len__()): spectrum[i][1].extend(s[i][1])

		return

	def compile_spectrum():
		import numpy as np
		nonlocal spectrum

		spectrum = [(_sr[0], np.mean(_sr[1]), np.std(_sr[1])) for _sr in spectrum_raw]

		return


	# main
	read_out_binary()
	compile_spectrum()

	return spectrum
