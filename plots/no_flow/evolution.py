def evolution(c0, **kwargs):
	# nonlocals
	alpha = None
	k_f_n2_a = None
	k_et = None
	k_no_n2 = None
	k_no_h2o = None
	n_concentrations = None
	concentrations = None


	# functions
	def arg(key):
		import sys

		# defaults
		defaults_constants = {
			'alpha': {'val': 1e-3, 'unit': 'Hz', 'source': 'guessed'},
			'k_f_n2_a': {'val': 5e-1, 'unit': 'Hz', 'source': 'literature'},
			'k_f_no_a': {'val': 5.155e6, 'unit': 'Hz', 'source': 'literature'},
			'tau_no': {'val': 194e-9, 'unit': 's', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'k_et': {'val': 2.471e9, 'unit': 'Hz', 'source': 'literature'},
			'p': {'val': 760, 'unit': 'Torr', 'source': 'atmospheric pressure'},
			'q_n2_n2': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_n2_o2': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_n2_no': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_n2_h2o': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_no_n2': {'val': .003e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'q_no_o2': {'val': 5.6e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'q_no_no': {'val': 8.17e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'q_no_h2o': {'val': 29.44e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			}

		arg_lists = [kwargs, defaults_constants]

		for arg_list in arg_lists:
			if key in arg_list.keys():
				return_value = arg_list[key]

				# default values
				if type(return_value) is dict:
					return_value = return_value['val']

				return return_value

		sys.stdout.write('Unable to fetch argument with name \'' + key + '\'.\nAborting program execution.\n')
		sys.exit(1)

		return

	def get_integration_constants():
		nonlocal alpha
		nonlocal k_f_n2_a
		nonlocal k_et
		nonlocal k_no_n2
		nonlocal k_no_h2o
		nonlocal n_concentrations

		alpha = arg('alpha')
		k_f_n2_a = arg('k_f_n2_a')
		k_et = arg('k_et')
		p = arg('p')
		k_no_n2 = arg('q_no_n2') * p
		k_no_h2o = arg('q_no_h2o') * p
		n_concentrations = c0.__len__()

		return

	def diff_t(c, t):
		ret = []

		n2x = c[0]
		n2a = c[1]
		nox = c[2]
		noa = c[3]
		h2o = c[4]

		n2x_p = -alpha * n2x + k_f_n2_a * n2a + k_et * n2a * nox
		n2a_p = alpha * n2x - k_f_n2_a * n2a - k_et * n2a * nox
		nox_p = -k_et * n2a * nox + k_no_n2 * noa * n2x + k_no_h2o * noa * h2o
		noa_p = k_et * n2a * nox - k_no_n2 * noa * n2x - k_no_h2o * noa * h2o
		h2o_p = 0

		ret.extend([n2x_p, n2a_p, nox_p, noa_p, h2o_p])

		return ret

	def integrate():
		import numpy as np
		from scipy.integrate import odeint as oi
		nonlocal concentrations

		t_start = arg('t_start')
		t_stop = arg('t_stop')
		n_steps = arg('n_steps')

		t = np.linspace(t_start, t_stop, num=n_steps)
		c = oi(diff_t, c0, t)

		concentrations = [[] for i in range(c0.__len__())]
		for _t, _c in zip(t, c):
			for i, __c in enumerate(_c):
				concentrations[i].append([_t, __c])


		return


	# main
	get_integration_constants()
	integrate()

	return concentrations
