def water_evolution(flow, **kwargs):
	# nonlocals
	lambda_pp = None
	lambda_plus = None
	lambda_minus = None
	c_h2o_n2 = None
	c1 = None
	c2 = None
	water_wall = None
	water_tube = None


	# functions
	def arg(key):
		import sys

		defaults_constants = {
			'c_h2o_0': {'val': 1, 'unit': '1', 'source': 'guessed'},
			'c_h2o_0_tube': {'val': None, 'unit': None, 'source': 'guessed'},
			'k_a': {'val': 1e3, 'unit': 'Hz', 'source': 'guessed'},
			'k_g': {'val': 1e3, 'unit': 'Hz', 'source': 'guessed'},
			'k_phi': {'val': 1e-3, 'unit': 'Hz / SLPM', 'source': 'guessed'},
			'c_h2o_n2': {'val': 0, 'unit': '1 / SLPM', 'source': 'guessed'},
			't': {'val': [i for i in range(1000)], 'unit': 's', 'source': 'guessed'}
			}

		arg_lists = [kwargs, defaults_constants]

		for arg_list in arg_lists:
			if key in arg_list.keys():
				return_value = arg_list[key]

				# default values
				if type(return_value) is dict:
					return_value = return_value['val']

				return return_value

		sys.stdout.write('Unable to fetch argument with name \'' + key + '\'.\nAborting program execution.\n')
		sys.exit(1)

		return None

	def compute_lambdas():
		import sys

		nonlocal lambda_plus, lambda_minus, lambda_pp

		if flow < 0.0:
			sys.stdout.write('Invalid value for the flow: ' + str(flow) + '.\nAborting program execution.\n\n')
			sys.exit()

		k_a = arg('k_a')
		k_g = arg('k_g')
		k_phi = arg('k_phi')

		lambda_p = (k_g + k_a + k_phi * flow) / 2
		lambda_pp = (lambda_p ** 2 - k_g * k_phi * flow) ** (1/2)
		lambda_plus = -lambda_p + lambda_pp
		lambda_minus = -lambda_p - lambda_pp
		# print('lambdas:', lambda_plus, lambda_minus, lambda_pp)

		return

	def get_c_h2o_n2():
		import sys
		nonlocal c_h2o_n2

		c_h2o_n2 = arg('c_h2o_n2')
		if c_h2o_n2 < 0:
			c_h2o_n2 = 0
			sys.stdout.write('Passed negative value for c_h2o_n2. Changed it to 0.\n')

			return

		k_g = arg('k_g')
		k_a = arg('k_a')
		max_val = min([1, k_g / k_a])

		if c_h2o_n2 > max_val:
			c_h2o_n2 = max_val
			sys.stdout.write('Passed value for c_h2o_n2 too large. Set it to ' + str(c_h2o_n2) + '.\n')

			return

		return

	def set_constants():
		# nonlocals
		c_wall = None
		c_tube = None


		# functions
		def get_initial_concentrations():
			nonlocal c_wall, c_tube

			c_wall = arg('c_h2o_0')
			c_tube = arg('c_h2o_0_tube')

			if c_tube is not None:
				return

			k_g = arg('k_g')
			k_a = arg('k_a')

			# c_tube = k_g / k_a * c_wall
			c_tube = c_wall

			return

		def get_initial_constants():
			nonlocal c1, c2

			k_g = arg('k_g')
			k_a = arg('k_a')

			bc1 = c_wall
			bc2 = c_tube

			if flow > 0:
				bc1 = c_wall - c_h2o_n2 / k_g * k_a
				bc2 = c_tube - c_h2o_n2


			c1 = (-(k_g + lambda_minus) * bc1 + k_a * bc2) / (2 * lambda_pp)
			c2 = ((k_g + lambda_plus) * bc1 - k_a * bc2) / (2 * lambda_pp)

			return


		# main
		get_initial_concentrations()
		get_initial_constants()

		return

	def get_water_wall():
		import math
		nonlocal water_wall

		t = arg('t')
		k_g = arg('k_g')
		k_a = arg('k_a')

		water_wall = []
		for _t in t:
			summand_1 = math.exp(lambda_plus * _t) * c1
			summand_2 = math.exp(lambda_minus * _t) * c2
			summand_3 = c_h2o_n2 / k_g * k_a
			water_wall.append(summand_1 + summand_2 + summand_3)

		return

	def get_water_tube():
		import math
		nonlocal water_tube

		water_tube = []
		t = arg('t')
		k_g = arg('k_g')
		k_a = arg('k_a')

		for _t in t:
			summand_1 = (k_g + lambda_plus) / (k_a) * math.exp(lambda_plus * _t) * c1
			summand_2 = (k_g + lambda_minus) / (k_a) * math.exp(lambda_minus * _t) * c2
			summand_3 = c_h2o_n2
			water_tube.append(summand_1 + summand_2 + summand_3)

		return


    # main
	compute_lambdas()
	get_c_h2o_n2()
	set_constants()
	get_water_wall()
	get_water_tube()

	return water_wall, water_tube
