def get_time_evolution(frac, gamma=1, n_it=100):
	# nonlocals
	c = 1 - frac
	increment = 1
	x = 10


	# functions
	def fun(_x):
		import math

		ret = (1 - math.exp(-_x)) / _x

		return ret

	def walk_up():
		nonlocal x

		while fun(x) > c:
			x = x + increment

		return

	def walk_down():
		nonlocal x

		while fun(x) < c:
			x = x - increment
			if x <= 0:
				x = increment

				return

		return


	# main
	for i in range(n_it):
		walk_up()
		walk_down()
		increment = increment / 2

	t0 = x / gamma

	return t0
