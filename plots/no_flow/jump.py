def jump(no, flows, **kwargs):
	# constants - integrate
	alpha = None
	k_f_n2_a = None
	k_et = None
	k_no_n2 = None
	k_no_h2o = None
	n_concentrations = None


	# nonlocals
	t_before = None
	t_after = None
	t_stops = None
	water = None
	concentrations = None
	radioluminescence = None


	# functions
	def arg(key):
		import sys

		# defaults
		defaults_constants = {
			'k_f_n2_a': {'val': 5e-1, 'unit': 'Hz', 'source': 'literature'},
			'k_f_no_a': {'val': 5.155e6, 'unit': 'Hz', 'source': 'literature'},
			'tau_no': {'val': 194e-9, 'unit': 's', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'k_et': {'val': 2.471e9, 'unit': 'Hz', 'source': 'literature'},
			'p': {'val': 760, 'unit': 'Torr', 'source': 'atmospheric pressure'},
			'q_n2_n2': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_n2_o2': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_n2_no': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_n2_h2o': {'val': 0, 'unit': 'Hz / Torr', 'source': 'None'},
			'q_no_n2': {'val': .003e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'q_no_o2': {'val': 5.6e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'q_no_no': {'val': 8.17e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'},
			'q_no_h2o': {'val': 29.44e6, 'unit': 'Hz / Torr', 'source': 'T. B. Settersten, B. D. Patterson (2015)'}
			}

		defaults_water = {
			'c_h2o_0': {'val': 1, 'unit': '1', 'source': 'guessed'},
			'c_h2o_0_tube': {'val': None, 'unit': None, 'source': 'guessed'},
			'k_a': {'val': 1e3, 'unit': 'Hz', 'source': 'guessed'},
			'k_g': {'val': 1e3, 'unit': 'Hz', 'source': 'guessed'},
			'k_phi': {'val': 1e-3, 'unit': 'Hz / SLPM', 'source': 'guessed'},
			'c_h2o_n2': {'val': 0, 'unit': '1', 'source': 'guessed'},
		}

		defaults_parameters = {
			'alpha': {'val': 1e-3, 'unit': 'Hz', 'source': 'guessed'},
			'n_steps': {'val': int(1e3), 'unit': '1', 'source': 'guessed'},
			'n_steps_after': {'val': int(1e2), 'unit': '1', 'source': 'guessed'},
			'c_init': {'val': [1, 0, 0, 0, 0], 'unit': 'n2x, n2a, nox, noa, h2o', 'source': 'no source condition'},
			't_scale': {'val': 1, 'unit': '1', 'source': 'no scaling condition'},
			't_start': {'val': 0, 'unit': 's', 'source': 'guessed'},
			't_jump': {'val': 1800, 'unit': 's', 'source': 'guessed'},
			't_end': {'val': 1900, 'unit': 's', 'source': 'guessed'},
			'display': {'val': None, 'unit': None, 'source': 'default'},
			'mode': {'val': 'photons', 'unit': None, 'source': 'default'},
		}

		arg_lists = [kwargs, defaults_constants, defaults_water, defaults_parameters]

		for arg_list in arg_lists:
			if key in arg_list.keys():
				return_value = arg_list[key]

				# default values
				if type(return_value) is dict:
					return_value = return_value['val']

				return return_value

		sys.stdout.write('Unable to fetch argument with name \'' + key + '\'.\nAborting program execution.\n')
		sys.exit(1)

		return

	def get_t_stops():
		nonlocal t_stops

		t_stop_1 = arg('t_stop_1')
		t_stops = [t_stop_1 / _f for _f in flows]

		return

	def split_times():
		nonlocal t_before
		nonlocal t_after

		t_end = arg('t_end')
		t_jump = arg('t_jump')
		t = [i for i in range(t_end)]
		t_before = t[:t_jump]
		t_after = t[t_jump:]
		t_after = [it - t_after[0] for it in t_after]

		return

	def get_water():
		nonlocal water

		water = []
		c_h2o_0 = arg('c_h2o_0')
		k_g = arg('k_g')
		k_a = arg('k_a')
		k_phi = arg('k_phi')
		c_h2o_n2 = arg('c_h2o_n2')

		for _f in flows:
			wall, tube = water_evolution(_f, t=t_before, c_h2o_n2=c_h2o_n2, k_g=k_g, k_a=k_a, c_h2o_0=c_h2o_0, k_phi=k_phi)
			_w = tube
			wall, tube = water_evolution(0, t=t_after, c_h2o_n2=c_h2o_n2, k_g=k_g, k_a=k_a, c_h2o_0=wall[-1], c_h2o_0_tube=tube[-1])
			_w.extend(tube)
			water.append(_w)

		return

	def get_integration_constants():
		nonlocal alpha
		nonlocal k_f_n2_a
		nonlocal k_et
		nonlocal k_no_n2
		nonlocal k_no_h2o
		nonlocal n_concentrations

		alpha = arg('alpha')
		k_f_n2_a = arg('k_f_n2_a')
		k_et = arg('k_et')
		p = arg('p')
		k_no_n2 = arg('q_no_n2') * p
		k_no_h2o = arg('q_no_h2o') * p
		n_concentrations = 5

		return

	def diff_t(c, t):
		ret = []

		for i in range(flows.__len__()):
			offset = n_concentrations * i
			n2x = c[offset + 0]
			n2a = c[offset + 1]
			nox = c[offset + 2]
			noa = c[offset + 3]
			h2o = c[offset + 4]

			n2x_p = -alpha * n2x + k_f_n2_a * n2a + k_et * n2a * nox
			n2a_p = alpha * n2x - k_f_n2_a * n2a - k_et * n2a * nox
			nox_p = -k_et * n2a * nox + k_no_n2 * noa * n2x + k_no_h2o * noa * h2o
			noa_p = k_et * n2a * nox - k_no_n2 * noa * n2x - k_no_h2o * noa * h2o
			h2o_p = 0

			ret.extend([n2x_p, n2a_p, nox_p, noa_p, h2o_p])

		return ret

	def get_photons(noa, n2x, h2o, **kwargs):
		k_f_no_a = arg('k_f_no_a')
		p = arg('p')
		k_no_n2 = arg('q_no_n2') * p
		k_no_h2o = arg('q_no_h2o') * p

		qe = k_f_no_a / (k_f_no_a + k_no_n2 * n2x + k_no_h2o * h2o)
		photons = qe * noa

		return photons

	def get_concentrations_and_radioluminescence():
		import sys
		import numpy as np
		from scipy.integrate import odeint as oi
		nonlocal concentrations
		nonlocal radioluminescence

		concentrations = [[] for i in range(flows.__len__())]
		radioluminescence = [[] for i in range(flows.__len__())]
		n_steps = arg('n_steps')
		n_steps_after = arg('n_steps_after')
		t_jump = arg('t_jump')
		display = arg('display')

		# get time and pos
		time = np.linspace(0, np.max(t_stops), num=n_steps)
		pos = []
		for _t in t_stops:
			for i in range(time.size):
				if time[i] >= _t:
					pos.append(i)

					break

		c = None
		t_end = water[0].__len__()
		t_start = arg('t_start')
		tot = t_end - t_start
		# before the jump
		for i in range(t_start, t_jump):
			if display is not None:
				sys.stdout.write(display + '{:.2f}'.format(100.0 * (i - t_start + 1) / tot) + '%')

			c_init = []
			[c_init.extend([1 - no - _w[i], 0, no, 0, _w[i]]) for _w in water]
			c = oi(diff_t, c_init, time)

			for j, _p in enumerate(pos):
				offset = 5 * j
				tar = c[_p]

				n2x = tar[offset + 0]
				noa = tar[offset + 3]
				h2o = tar[offset + 4]

				concentrations[j].append(tar[offset:offset + 5])
				radioluminescence[j].append(get_photons(noa, n2x, h2o))

		# after the jump
		c_before = []
		[c_before.extend(c[_p][5 * i:5 * (i + 1)]) for i, _p in enumerate(pos)]

		for i in range(t_jump, t_end):
			if display is not None:
				sys.stdout.write(display + '{:.2f}'.format(100.0 * (i - t_start + 1) / tot) + '%')

			# set new water concentrations
			for j, _w in enumerate(water):
				offset = j * 5
				c_before[offset + 4] = _w[i]

			time_one_sec = np.linspace(0, 1, num=n_steps_after)
			c_after = oi(diff_t, c_before, time_one_sec)[-1]

			for j in range(flows.__len__()):
				offset = 5 * j
				n2x = c_after[offset + 0]
				noa = c_after[offset + 3]
				h2o = c_after[offset + 4]

				concentrations[j].append(tar[offset:offset + 5])
				radioluminescence[j].append(get_photons(noa, n2x, h2o))

			c_before = c_after

		return


	# main
	get_t_stops()
	split_times()
	get_water()
	get_integration_constants()
	get_concentrations_and_radioluminescence()

	mode = arg('mode')
	if mode == 'concentrations':
		return concentrations

	if mode == 'photons':
		return radioluminescence

	return
