import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="plots",
    version="2.0.0",
    author="Thomas Kerst",
    author_email="thomas@huchma.fi",
    description="Library to generate beautiful plots",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/kerst-thomas-projects/plots",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
