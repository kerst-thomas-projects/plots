# Plotting Library

Python (3.8) library for creating beautiful plots


## Building Instructions

```shell
make
make install
```

## Example Usage

```python
import plots

print(plots.__version__)
print(plots.system.sep())
```


## Functions

| Function Family | Functions |
| - | - |
| [bin](https://gitlab.com/kerst-thomas-projects/plots/tree/master/plots/bin) | `write_list(path, data)`<br>`read_list(path, elem_len)`<br>`write_lists_of_int(path, data)`<br>`read_lists_of_int(path, scaling=1, dime_divisor=False)`<br>`read_spectrum(path)` |
| [calibrate](https://gitlab.com/kerst-thomas-projects/plots/tree/master/calibrate/bin) | `grating(raw, grating_id)`                                   |
| [fit](https://gitlab.com/kerst-thomas-projects/plots/tree/master/fit/bin) | `prop(data)`<br>`lin(data)`<br>`get_r2(data, fun)`<br>`get_chi_squared(vals, func, ddof=0)`<br>`no_radioluminescence(data, alpha=1e-6)`<br>`cherenkov(data, ri_path, x_plot=None)`<br>`no_buildup(data, flow, params_init_passed=None, do_check=False, log_path=None)`<br>`no_jump(data, flows, boundaries=None, do_check=None, log_path=None, constants=None, **p0)` |
| [img_processing](https://gitlab.com/kerst-thomas-projects/plots/tree/master/img_processing/bin) | `show(img, t=1.0)`<br>`overlay(path, bg_name='background.bmp', background_scaling_factor=0, cutoff=250, gamma_removal_window_width=2, overlay_col=[1, 0, 0])`<br>`overlay_cv2(path_src, path_tar, background_scaling_factor=0, centre=(0, 0), collection_radius=0)` |
| [integrate](https://gitlab.com/kerst-thomas-projects/plots/tree/master/integrate/bin) | `integrate(x, y)`                                            |
| [no_flow](https://gitlab.com/kerst-thomas-projects/plots/tree/master/no_flow/bin) | `water_evolution(flow, **kwargs)`<br>`jump(no, flows, **kwargs)`<br>`evolution(c0, **kwargs)`<br>`get_time_evolution(frac, gamma=1, n_it=100)` |
| [plotting](https://gitlab.com/kerst-thomas-projects/plots/tree/master/plotting/bin) | `xy(data, twinx=None, **kwargs)`<br>`heat_map(data, save_path=None, vmin=None, vmax=None, use_colorbar=False)`<br>`heat_map_with_overlay(data, overlays, save_path=None, vmin=None, vmax=None, use_colorbar=False)` |
| [pmt](https://gitlab.com/kerst-thomas-projects/plots/tree/master/pmt/bin) | `read(f_path)`                                               |
| [signal](https://gitlab.com/kerst-thomas-projects/plots/tree/master/signal/bin) | `walk(input, **kwargs)`<br>`mean(input, filter_width=1)`<br>`median(input, filter_width=1)` |
| [system](https://gitlab.com/kerst-thomas-projects/plots/tree/master/system/bin) | `get_sep()`<br>`sep()`<br>`get_four_bytes_specificer()`<br>`erase_line()`<br>`erase_last_n_chars()`<br> |
| [txt](https://gitlab.com/kerst-thomas-projects/plots/tree/master/txt/bin) | `fetch_new_line_sep_vals(path)`<br>`fetch_sep(path, sep)`<br>`fetch_pmt(path)`<br>`save_to_csv(path, list, header)` |
