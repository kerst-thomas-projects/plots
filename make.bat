: dependencies
python3 -m pip install -U --user install matplotlib

: actual installation
python3 setup.py sdist
python3 -m pip install -U --user --force-reinstall dist\plots-2.0.0.tar.gz