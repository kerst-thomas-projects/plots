.PHONY: default build install

default: build

build:
	rm -rf build &&  \
	python3 setup.py sdist bdist_wheel

install:
	python3 -m pip install -U --user --force-reinstall dist/plots-2.0.0-py3-none-any.whl
